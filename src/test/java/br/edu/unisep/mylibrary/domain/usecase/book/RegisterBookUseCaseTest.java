package br.edu.unisep.mylibrary.domain.usecase.book;

import br.edu.unisep.mylibrary.data.entity.book.Book;
import br.edu.unisep.mylibrary.data.repository.BookRepository;
import br.edu.unisep.mylibrary.domain.builder.book.BookBuilder;
import br.edu.unisep.mylibrary.domain.dto.book.RegisterBookDto;
import br.edu.unisep.mylibrary.domain.validator.book.RegisterBookValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class RegisterBookUseCaseTest {

    private RegisterBookUseCase useCase;

    private RegisterBookValidator validator;
    private BookBuilder builder;
    private BookRepository bookRepository;

    @BeforeEach
    void setup() {
        this.validator = mock(RegisterBookValidator.class);
        this.builder = mock(BookBuilder.class);
        this.bookRepository = mock(BookRepository.class);

        this.useCase = new RegisterBookUseCase(validator, builder, bookRepository);
    }

    @Test
    void shouldFindBookInvalid() {
        var registerBook = mock(RegisterBookDto.class);
        doThrow(new IllegalArgumentException("Mock message")).when(validator).validate(any());

        assertThrows(IllegalArgumentException.class, () -> useCase.execute(registerBook), "Mock message");

        verify(builder, times(0)).from(any(RegisterBookDto.class));
        verify(bookRepository, times(0)).save(any(Book.class));
    }

    @Test
    void shouldSaveBook() {
        var registerBook = mock(RegisterBookDto.class);

        when(builder.from(any(RegisterBookDto.class))).thenReturn(mock(Book.class));

        useCase.execute(registerBook);

        verify(validator, times(1)).validate(any(RegisterBookDto.class));
        verify(builder, times(1)).from(any(RegisterBookDto.class));
        verify(bookRepository, times(1)).save(any(Book.class));
    }
}
