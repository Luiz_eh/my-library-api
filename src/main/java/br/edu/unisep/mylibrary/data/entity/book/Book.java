package br.edu.unisep.mylibrary.data.entity.book;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "books")
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "book_id")
    private Integer id;

    @Column(name = "title")
    private String title;

    @Column(name = "author")
    private String author;

    @Column(name = "edition")
    private Integer edition;

    @Column(name = "publication_year")
    private Integer publicationYear;

    @Column(name = "summary")
    private String summary;

    @Column(name = "publisher")
    private String publisher;

    @Column(name = "pages")
    private Integer pages;

    @Column(name = "isbn")
    private String isbn;

}
