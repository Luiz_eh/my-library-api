package br.edu.unisep.mylibrary.controller.book;

import br.edu.unisep.mylibrary.domain.dto.book.BookDto;
import br.edu.unisep.mylibrary.domain.dto.book.RegisterBookDto;
import br.edu.unisep.mylibrary.domain.dto.book.UpdateBookDto;
import br.edu.unisep.mylibrary.domain.usecase.book.*;
import br.edu.unisep.mylibrary.response.DefaultResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/book")
public class BookController {

    private final FindAllBooksUseCase findAllBooks;
    private final FindBookByIdUseCase findById;
    private final RegisterBookUseCase registerBook;
    private final UpdateBookUseCase updateBook;
    private final FindBookByIsbnUseCase findBookByIsbn;
    private final FindBookByTitleUseCase findBookByTitle;

    @GetMapping
    public ResponseEntity< DefaultResponse<List<BookDto>> > findAll() {
        var books = findAllBooks.execute();

        return books.isEmpty() ?
                ResponseEntity.noContent().build() :
                ResponseEntity.ok(DefaultResponse.of(books));
    }

    @GetMapping("/{id}")
    public ResponseEntity<DefaultResponse<BookDto>> findById(@PathVariable("id") Integer id) {
        var book = findById.execute(id);

        return book == null ?
                ResponseEntity.notFound().build() :
                ResponseEntity.ok(DefaultResponse.of(book));
    }

    @PostMapping
    public ResponseEntity<DefaultResponse<Boolean>> save(@RequestBody RegisterBookDto book) {
        registerBook.execute(book);
        return ResponseEntity.ok(DefaultResponse.of(true));
    }

    @PutMapping
    public ResponseEntity<DefaultResponse<Boolean>> update(@RequestBody UpdateBookDto book) {
        updateBook.execute(book);
        return ResponseEntity.ok(DefaultResponse.of(true));
    }

    @GetMapping("/byIsbn/{isbn}")
    public ResponseEntity<DefaultResponse<BookDto>> findByIsbn(@PathVariable("isbn") String isbn) {
        var book = findBookByIsbn.execute(isbn);

        return book == null ?
                ResponseEntity.notFound().build() :
                ResponseEntity.ok(DefaultResponse.of(book));
    }

    @GetMapping("/byTitle")
    public ResponseEntity<DefaultResponse<List<BookDto>>> findByTitle(@RequestParam("title") String title) {
        var books = findBookByTitle.execute(title);

        return books.isEmpty() ?
                ResponseEntity.noContent().build() :
                ResponseEntity.ok(DefaultResponse.of(books));
    }


}
