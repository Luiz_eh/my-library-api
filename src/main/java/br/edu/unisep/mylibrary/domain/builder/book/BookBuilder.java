package br.edu.unisep.mylibrary.domain.builder.book;

import br.edu.unisep.mylibrary.data.entity.book.Book;
import br.edu.unisep.mylibrary.domain.dto.book.BookDto;
import br.edu.unisep.mylibrary.domain.dto.book.RegisterBookDto;
import br.edu.unisep.mylibrary.domain.dto.book.UpdateBookDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class BookBuilder {

    public List<BookDto> from(List<Book> books) {
        return books.stream().map(this::from).collect(Collectors.toList());
    }

    public BookDto from(Book book) {
        return new BookDto(
                book.getId(),
                book.getTitle(),
                book.getAuthor(),
                book.getEdition(),
                book.getPublicationYear(),
                book.getSummary(),
                book.getPublisher(),
                book.getPages(),
                book.getIsbn()
        );
    }

    public Book from(UpdateBookDto updateBook) {

        Book book = from((RegisterBookDto) updateBook);
        book.setId(updateBook.getId());

        return book;
    }

    public Book from(RegisterBookDto registerBook) {
        Book book = new Book();
        book.setTitle(registerBook.getTitle());
        book.setAuthor(registerBook.getAuthor());
        book.setEdition(registerBook.getEdition());
        book.setPublicationYear(registerBook.getPublicationYear());
        book.setSummary(registerBook.getSummary());
        book.setPublisher(registerBook.getPublisher());
        book.setPages(registerBook.getPages());
        book.setIsbn(registerBook.getIsbn());

        return book;
    }

}
