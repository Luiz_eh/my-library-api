package br.edu.unisep.mylibrary.domain.usecase.book;

import br.edu.unisep.mylibrary.data.repository.BookRepository;
import br.edu.unisep.mylibrary.domain.builder.book.BookBuilder;
import br.edu.unisep.mylibrary.domain.dto.book.RegisterBookDto;
import br.edu.unisep.mylibrary.domain.validator.book.RegisterBookValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RegisterBookUseCase {

    private final RegisterBookValidator validator;
    private final BookBuilder builder;
    private final BookRepository bookRepository;

    public void execute(RegisterBookDto registerBook) {
        validator.validate(registerBook);

        var book = builder.from(registerBook);
        bookRepository.save(book);
    }

}
