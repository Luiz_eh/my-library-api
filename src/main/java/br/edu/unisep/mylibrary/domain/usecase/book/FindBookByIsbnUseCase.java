package br.edu.unisep.mylibrary.domain.usecase.book;

import br.edu.unisep.mylibrary.data.repository.BookRepository;
import br.edu.unisep.mylibrary.domain.builder.book.BookBuilder;
import br.edu.unisep.mylibrary.domain.dto.book.BookDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class FindBookByIsbnUseCase {

    private final BookRepository bookRepository;
    private final BookBuilder builder;

    public BookDto execute(String isbn) {
        var book = bookRepository.findByIsbn(isbn);
        return book.map(builder::from).orElse(null);
    }

}
